import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import com.w4soln.bean.Customer;

public class MainApplication {

	interface TriConsumer<T, U, V> {
		public void accept(T t, U u, V v);
	}

	public static void main(String[] args) throws NumberFormatException, IOException {
		List<Customer> customers = new ArrayList<>();
		customers.add(new Customer(51651668L, "rohit123"));
		customers.add(new Customer(16519831L, "rahul123"));
		customers.add(new Customer(81650150L, "virat123"));
		customers.add(new Customer(26759896L, "rishabh123"));
		customers.add(new Customer(30005484L, "dhoni123"));

		FileWriter fileWriter = new FileWriter("transactions.txt", true);
		PrintWriter printWriter = new PrintWriter(fileWriter);

		Map<Long, Double> customerAccounts = new HashMap<Long, Double>();

		Consumer<Customer> initializeAccount = (customer) -> customerAccounts.put(customer.getBankAccountNo(), 0.0);

		Function<Long, Optional<Customer>> getCustomer = (accNo) -> {
			return customers.stream().filter(cust -> cust.getBankAccountNo() == accNo).findFirst();
		};

		BiConsumer<Customer, Double> deposit = (customer, amount) -> {
			Double balance = customerAccounts.get(customer.getBankAccountNo());
			balance += amount;
			customerAccounts.put(customer.getBankAccountNo(), balance);
			System.out.println(customer.getBankAccountNo() + ": Deposit Successful. Updated balance is " + balance);
			printWriter.println(customer.getBankAccountNo() + ": Deposit Successful. Updated balance is " + balance);
		};

		BiConsumer<Customer, Double> withdrawal = (customer, amount) -> {
			Double balance = customerAccounts.get(customer.getBankAccountNo());
			if (amount <= balance) {
				balance -= amount;
				customerAccounts.put(customer.getBankAccountNo(), balance);
				System.out.println(
						customer.getBankAccountNo() + ": Withdrawal Successful. Updated balance is " + balance);
				printWriter.println(
						customer.getBankAccountNo() + ": Withdrawal Successful. Updated balance is " + balance);

			} else {
				System.out.println(customer.getBankAccountNo() + ": Withdrawal failed. Insufficient amount");
			}
		};

		Supplier<Integer> generate4DigitOTP = () -> (int) (Math.random() * 9000) + 1000;

		TriConsumer<Customer, Customer, Double> transfer = (fromCustomer, toCustomer, amount) -> {
			Double balance = customerAccounts.get(fromCustomer.getBankAccountNo());
			if (amount <= balance) {
				customerAccounts.put(fromCustomer.getBankAccountNo(), balance - amount);
				balance = customerAccounts.get(toCustomer.getBankAccountNo());
				customerAccounts.put(toCustomer.getBankAccountNo(), balance + amount);
				System.out.println(fromCustomer.getBankAccountNo() + ": Transfer Successful. Updated balance is "
						+ (balance - amount));
				printWriter.println(fromCustomer.getBankAccountNo() + ": Transfer Successful. Updated balance is "
						+ (balance - amount));
			} else {
				System.out.println(fromCustomer.getBankAccountNo() + ": Transfer failed. Insufficient amount");
				printWriter.println(fromCustomer.getBankAccountNo() + ": Transfer failed. Insufficient amount");
			}

		};

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		customers.forEach(cust -> initializeAccount.accept(cust));
		while (true) {
			System.out.println("Enter account number");
			Long acc = Long.parseLong(br.readLine());
			Optional<Customer> opCustomer = getCustomer.apply(acc);
			if (!opCustomer.isPresent()) {
				System.out.println("Account doesn't exist");
			} else {
				Customer currCustomer = opCustomer.get();
				System.out.println("Enter password");
				String pass = br.readLine();
				if (pass.equals(currCustomer.getPassword())) {
					OUTER: while (true) {

						System.out.println("!!!!! Welcome to Indian Bank !!!!!\n");
						System.out.println("-------------------------------------");
						System.out.println("Enter the operation you want to perform");
						System.out.println("1. Deposit\n2. Withdrawal\n3. Transfer\n4. Logout");
						System.out.println("-------------------------------------");
						int choice = Integer.parseInt(br.readLine());
						Double amt;
						switch (choice) {
						case 1:
							System.out.println("Enter Amount");
							amt = Double.parseDouble(br.readLine());
							deposit.accept(currCustomer, amt);

							break;
						case 2:
							System.out.println("Enter Amount");
							amt = Double.parseDouble(br.readLine());
							withdrawal.accept(currCustomer, amt);
							break;
						case 3:
							System.out.println("Your OTP is:");
							Integer myOtp = generate4DigitOTP.get();
							System.out.println(myOtp);
							System.out.println("Enter OTP");
							Integer otp = Integer.parseInt(br.readLine());
							if (!otp.equals(myOtp)) {
								System.out.println("OTP verification failed. Returning to menu.");
							} else {
								System.out.println("Enter Amount");
								amt = Double.parseDouble(br.readLine());
								System.out.println("Enter Amount to which you want to transfer.");
								Long toAcc = Long.parseLong(br.readLine());
								Optional<Customer> toCustomer = getCustomer.apply(toAcc);
								if (toCustomer.isPresent()) {
									transfer.accept(currCustomer, toCustomer.get(), amt);
								} else {
									System.out.println("Transfer failed. Account provided does not exist");
								}
							}
							break;
						case 4:
							System.out.println("Exited Successfully");
							break OUTER;
						default:
							System.out.println("Wrong choice. Try again");
						}
					}

				} else {
					System.out.println("Wrong Credentials.");
				}

			}

		}

	}

}
